#!/bin/sh
# Time-stamp: < l10n.sh (2017-07-05 08:41) >

# Copyright (C) 2009-2020 Martin Slouf <martinslouf@users.sourceforge.net>
#
# This file is a part of Summer.
#
# Summer is free software; you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation; either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

# Sample custom made script for generating gettext runtime resources.  Will
# probably require some modifications to suit your environment.

lang="cs"
if test -n "$1"; then
    lang=$1
fi
l10ndir="${lang}/LC_MESSAGES"
domain="tmplproject"

pygettext3 --add-location `find .. -name "*.py" -print`

cat messages.pot | sed \
-e "s/SOME DESCRIPTIVE TITLE/${domain} [${lang}]/" \
-e 's/YEAR ORGANIZATION/2009-2016 Martin Šlouf/' \
-e 's/FIRST AUTHOR <EMAIL@ADDRESS>, YEAR/Martin Šlouf <martinslouf@sf.net>, 2009-2016/' \
-e 's/FULL NAME <EMAIL@ADDRESS>/Martin Šlouf <martinslouf@sf.net>/' \
-e 's/PACKAGE VERSION/1.0/' \
-e "s/LANGUAGE <LL/Czech <${lang}/" \
-e 's/CHARSET/UTF-8/' \
-e 's/ENCODING/UTF-8/' \
>${domain}_${lang}.pot

cat >> ${domain}_${lang}.pot <<EOF
#
# Locale Variables:
# coding: utf-8
# End:
#
EOF

$EDITOR ${domain}_${lang}.pot

mkdir -p $l10ndir
msgfmt --output-file=${l10ndir}/${domain}.mo ${domain}_${lang}.pot
