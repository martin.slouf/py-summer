#!/bin/sh
# Time-stamp: < sample.local.sh (2019-09-30 12:58) >

# Copyright (C) 2009-2020 Martin Slouf <martinslouf@users.sourceforge.net>
#
# This file is a part of Summer.
#
# Summer is free software; you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation; either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

# initializes the ldap database
#
# - run as root, the LDAP database should be properly set up see accompanying
#   ./sample.local.ldif
#
# - database content will be replaced!
#
# - make sure all the files are owned by the proper (LDAP) user
#
# - filesystem path's in this script may need modification
#
# - you can create encrypted passwords with:
#   perl -e 'print("userPassword: {CRYPT}".crypt("secret","salt")."\n");'

if test $(id -u) != "0"; then
    echo "E: run as root"
    exit 1
fi

# modify to your abs path's
db_dir="/var/lib/ldap-db/sample.local"
ldap_diff="" # if not file, use "here document" in this file
dc="dc=sample,dc=local"

# create the storage area
systemctl stop slapd.service
rm -rf $db_dir/*

# start & stop the server -- this will create necessary db laypout,
# if server is properly set up
systemctl start slapd.service
systemctl stop slapd.service

# init the data (we must use offline method)
if test -f "$ldap_diff"; then
    slapadd -b $dc -v -s -q -l $ldap_diff
else
    slapadd -b $dc -v <<EOF
dn: $dc
objectClass: top
objectClass: dcObject
objectClass: organization
o: sample
dc: sample

dn: cn=admin,$dc
objectClass: simpleSecurityObject
objectClass: organizationalRole
cn: admin
description: LDAP administrator
# password is 'secret'
userPassword: {CRYPT}saHW9GdxihkGQ

dn: ou=users,$dc
objectClass: organizationalUnit
objectClass: top
ou: users

dn: ou=users-ftp,$dc
objectClass: organizationalUnit
objectClass: top
ou: users-ftp

dn: cn=Elephant,ou=users,$dc
objectClass: simpleSecurityObject
objectClass: organizationalRole
cn: Elephant
description: principal
# password is 'xxx'
userPassword: {CRYPT}saxfpWOeJG9wo

dn: cn=Lion,ou=users,$dc
objectClass: simpleSecurityObject
objectClass: organizationalRole
cn: Lion
description: principal
# password is 'xxx'
userPassword: {CRYPT}saxfpWOeJG9wo

dn: cn=Zebra,ou=users-ftp,$dc
objectClass: simpleSecurityObject
objectClass: organizationalRole
cn: Zebra
description: principal
# password is 'xxx'
userPassword: {CRYPT}saxfpWOeJG9wo

EOF
fi

# chown
chown openldap:openldap -R $db_dir

# start the server
systemctl start slapd.service

# dump db to console
ldapsearch -x -D "cn=admin,dc=sample,dc=local" -w secret -b "dc=sample,dc=local"
