# Time-stamp: <2021-02-20 11:13:01 martin>

# Copyright (C) 2009-2020 Martin Slouf <martinslouf@users.sourceforge.net>
#
# This file is a part of Summer.
#
# Summer is free software; you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation; either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

# GNU Makefile, uses common UNIX & Python tools (find, sed, twine)

PKG_NAME=summer
TARGET_ARCHIVE=dist/$(PKG_NAME)-$(VERSION).tar.gz
SF_REPO=ssh://martinslouf@git.code.sf.net/p/py-summer/code
FROM=$(SF_REPO)/trunk
TO=$(SF_REPO)/tags/release-$(VERSION)

SETUP_PY=$(shell cat setup.py)
SETUP_PY_VERSION=$(findstring $(VERSION), $(SETUP_PY))

PYTHON=$(shell which python)
SPHINXBUILD=$(shell which sphinx-build)
TWINE=$(shell which twine)

all:
	@echo "legal targets: 'clean', 'doc', 'tags', 'release', 'test', 'sloccount'"

clean:
	@echo "cleaning object files"
	find . -name "*~" -delete
	find . -name "*.pyc" -delete
	find . -name "*.pyo" -delete
	find . -name "*.log" -delete
	find . -name "__pycache__" -delete
	rm -rf dist MANIFEST summer.egg-info
	rm -f TAGS OOBR
	cd sphinx && make clean

doc: doc/.buildinfo

doc/.buildinfo:
	@echo "generating documentation from python source"
	cd sphinx && make clean html SPHINXBUILD="$(SPHINXBUILD)" && cp --archive build/html ../doc

tags:
	@echo "generating tags for GNU Emacs"
	etags -o TAGS `find . -name "*.py"`
#	ootags -o OOBR `find . -name "*.py"`

sdist: clean sdist-check $(TARGET_ARCHIVE)
	make $(TARGET_ARCHIVE)

sdist-check:
	@echo "creating release"
ifeq ($(strip $(VERSION)),)
	$(error "please define VERSION")
else
	@echo "VERSION=$(VERSION)"
	@echo "TARGET_ARCHIVE=$(TARGET_ARCHIVE)"
endif

$(TARGET_ARCHIVE):
	python setup.py sdist

release: clean test pre-release-check sdist git-tag sf-upload sf-web-upload pypi-upload

pre-release-check:
	@echo "pre-release checks"
#	script input
ifeq ($(and \
$(strip $(VERSION)), \
$(strip $(SF_USERNAME)) \
),)
	$(error "please define VERSION, SF_USERNAME")
else
	$(info "VERSION=$(VERSION)")
	$(info "SF_USERNAME=$(SF_USERNAME)")
	$(info "TARGET_ARCHIVE=$(TARGET_ARCHIVE)")
endif

#	setup.py version
ifeq ($(SETUP_PY_VERSION),$(VERSION))
	$(info "SETUP_PY_VERSION=$(SETUP_PY_VERSION)")
else
	$(error "please define correct version in setup.py")
endif

git-tag: $(TARGET_ARCHIVE)
	git status
	@echo "git tag 'release-$(VERSION)' will be created"
	@echo "Ctrl-C to abort. Continue?"
	@read continue
	git tag release-$(VERSION)
	for i in origin sf-origin ; do git push $i && git push --tags $i ; done

sf-upload: $(TARGET_ARCHIVE)
	@echo "file '$(notdir $(TARGET_ARCHIVE))' will be uploaded to SourceForge"
	@echo "Ctrl-C to abort. Continue?"
	@read continue
	rsync -avP -e ssh $(TARGET_ARCHIVE) \
		$(SF_USERNAME)@frs.sourceforge.net:/home/frs/project/py-summer/
	rsync -avP -e ssh README.txt \
		$(SF_USERNAME)@frs.sourceforge.net:/home/frs/project/py-summer/

sf-web-upload: doc $(TARGET_ARCHIVE)
	@echo "web will be uploaded to SourceForge"
	@echo "Ctrl-C to abort. Continue?"
	@read continue
	rsync -avP --delete --recursive -e ssh doc/ \
		$(SF_USERNAME)@web.sourceforge.net:/home/project-web/py-summer/htdocs

pypi-upload: $(TARGET_ARCHIVE)
	@echo "$(PKG_NAME) will be uploaded to PyPI"
	@echo "Ctrl-C to abort. Continue?"
	@read continue
	$(TWINE) upload -r pypi-legacy dist/*

test:
	@echo "running tests using tox"
	. .venv/bin/activate ; tox ; deactivate

sloccount:
	@echo "computing development effort"
	sloccount --follow summer
