Producer--Consumer
===================

**pc** -- generic producer--consumer
------------------------------------
.. automodule:: summer.pc

**pcg** -- producer--consumer with generator
--------------------------------------------
.. automodule:: summer.pcg
