Miscellaneous modules
=====================

**utils** -- utilities
----------------------
.. automodule:: summer.utils

**stringutils** -- string utilities
-----------------------------------
.. automodule:: summer.stringutils

**ipythonutils** -- ipython debugging support
---------------------------------------------
.. automodule:: summer.ipythonutils

**ass** -- assert
-----------------
.. automodule:: summer.ass

**lod** -- list of dictionaries
-------------------------------
.. automodule:: summer.lod
