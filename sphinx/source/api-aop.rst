AOP modules
===========

**aop** -- declarative transactions and sessions
------------------------------------------------
.. automodule:: summer.aop
