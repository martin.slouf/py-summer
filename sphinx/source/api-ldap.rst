LDAP -- lightweight directory access)
=====================================

**lsf** -- LDAP session factory
-------------------------------
.. automodule:: summer.lsf

**ldapdao** -- LDAP data access object support
----------------------------------------------
.. automodule:: summer.ldapdao
