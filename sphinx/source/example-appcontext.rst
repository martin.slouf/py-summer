Basic Application Context
=========================
.. automodule:: summer.tests.examples.appcontext

conf.py
-------
.. literalinclude:: ../../summer/tests/examples/appcontext/conf.py

appcontexttest.py
-----------------
.. literalinclude:: ../../summer/tests/examples/appcontext/appcontexttest.py

appcontext.py
-------------
.. literalinclude:: ../../summer/tests/examples/appcontext/appcontext.py
