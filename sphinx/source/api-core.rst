Core modules
============

**ex** -- exception definitions
-------------------------------
.. automodule:: summer.ex

**domain** -- domain model
--------------------------
.. automodule:: summer.domain

**context** -- summer object container
--------------------------------------
.. automodule:: summer.context

**l10n** -- localization
------------------------
.. automodule:: summer.l10n
