Real World template project
===========================
.. automodule:: summer.tests.examples.tmplproject

conf.py
-------
.. literalinclude:: ../../summer/tests/examples/tmplproject/tmplproject/conf.py

tmplprojecttest.py
------------------
.. literalinclude:: ../../summer/tests/examples/tmplproject/tmplprojecttest.py

tmplproject/main.py
-------------------
.. literalinclude:: ../../summer/tests/examples/tmplproject/tmplproject/main.py

tmplproject/appcontext.py
-------------------------
.. literalinclude:: ../../summer/tests/examples/tmplproject/tmplproject/appcontext.py
