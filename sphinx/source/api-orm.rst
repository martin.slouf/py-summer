ORM -- object relational mapping
================================

**sf** -- ORM session factory
-----------------------------
.. automodule:: summer.sf

**dao** -- ORM data access object support
-----------------------------------------
.. automodule:: summer.dao
