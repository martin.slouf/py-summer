summer
======

Summer is light weight Python 3 application framework to support generic
application development.  It provides support for business object
management, ORM (mapping, declarative transactions), LDAP and localization.
Inspired by famous Java Spring application framework.

Please use `SourceForge project page
<https://sourceforge.net/projects/py-summer/>`_ to download the source code
& report bugs.

**Quick installation**::

  pip install summer

**Contents**:

.. toctree::
   :maxdepth: 1

   readme
   basic-concepts
   examples
   api

Introduction
============

Please see :doc:`readme` and :doc:`basic-concepts` for introduction.  There
are some *real world* :doc:`examples`, which require source code
reading. The rest of this doc is *sphinx* generated API reference.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
